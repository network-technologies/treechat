
public class Timer extends Thread {
    private static int INTERVAL = 1;
    private Integer receivedBytes;
    private long fileSize;

    public Timer(Integer currBytes, long fileSize) {
        receivedBytes = currBytes;
        this.fileSize = fileSize;
    }

    @Override
    public void run() {
        int i = 0;
        double current = 0.0, average, temp = 0.0;
        Connection.setTemp(0);
        System.out.println("timer is running");

        while (Connection.getReceiveStatus()) {
            long startPoint = System.nanoTime();
            try {
                Thread.sleep(3000);
                i++;

                if (((System.nanoTime() - startPoint)) % INTERVAL == 0)
                    current = 1.0 * (Connection.getTotal() - Connection.getTemp()) / INTERVAL / 1024 / 1024;
                temp += current;
                average = temp / i;
                Connection.setTemp(Connection.getTotal());
                System.out.format("current speed %.2f Mb/s\n", current);
                System.out.format("average speed %.2f Mb/s\n\n", average);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
        }
    }
}